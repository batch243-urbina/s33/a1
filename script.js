// console.log(`Hello`);

// 1. FETCH REQUEST
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((json) => console.log(json));

// // MAP Method

const titleArray = [];

fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((data) => {
    data.map((list) => {
      titleArray.push(list.title);
    });
  });
console.log(titleArray);

// SINGLE TO DO ITEM LIST and PRINT TITLE AND STATUS
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  .then((json) => console.log(json));

// PRINT MESSAGE PROVIDING TITLE AND STATUS
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  .then((json) =>
    console.log(
      `The title of the todo is "${json.title}" and it's status is ${json.completed}`
    )
  );

// FETCH USING POST THAT WILL CREATE A TO DO LIST ITEM
fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    title: "New to do list item",
    completed: false,
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// FETCH USING PUT THAT WILL UPDATE A TO DO LIST ITEM
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    title: "Updated to do list with added data structure",
    description: "Add data structure and update item",
    status: "false",
    dateCompleted: "pending",
    userId: 1,
    id: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// FETCH USING PATCH THAT WILL UPDATE A TO DO LIST ITEM
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    completed: true,
    dateCompleted: "11/30/22",
    userId: 1,
    id: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// FETCH USING DELETE METHOD
fetch("https://jsonplaceholder.typicode.com/todos/2", {
  method: "DELETE",
})
  .then((response) => response.json())
  .then((json) => console.log(json));
